%global _empty_manifest_terminate_build 0
Name:		python-keras-rl2
Version:	1.0.5
Release:	2
Summary:	Deep Reinforcement Learning for Tensorflow 2 Keras
License:	MIT
URL:		https://github.com/wau/keras-rl2
Source0:	https://files.pythonhosted.org/packages/a1/0a/7e980f921a51cd75a8cf887fa3ac6a7e3f3b2bc22f45549e90cfaba37ed0/keras-rl2-1.0.5.tar.gz
BuildArch:	noarch

Requires:	python3-tensorflow
Requires:	python3-gym

%description
keras-rl2 implements some state-of-the art deep reinforcement learning algorithms in Python and seamlessly integrates with the deep learning library Keras.

%package -n python3-keras-rl2
Summary:	Deep Reinforcement Learning for Tensorflow 2 Keras
Provides:	python-keras-rl2
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-keras-rl2
keras-rl2 implements some state-of-the art deep reinforcement learning algorithms in Python and seamlessly integrates with the deep learning library Keras.

%package help
Summary:	Development documents and examples for keras-rl2
Provides:	python3-keras-rl2-doc
%description help
keras-rl2 implements some state-of-the art deep reinforcement learning algorithms in Python and seamlessly integrates with the deep learning library Keras.

%prep
%autosetup -n keras-rl2-1.0.5

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-keras-rl2 -f filelist.lst
%dir %{python3_sitelib}/*
%exclude %{python3_sitelib}/tests

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed May 31 2023 zhangruifang2020 <zhangruifang1@h-partners.com> - 1.0.5-2
- Remove tests files

* Tue Jun 21 2022 chendexi <chendexi@kylinos.cn> - 1.0.5-1
- Upgrade to version v1.0.5 

* Thu Oct 15 2020 Python_Bot <Python_Bot@openeuler.org> - 1.0.4-1
- Package Spec generated
